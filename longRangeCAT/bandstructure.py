import sys
sys.path.insert(0, '..') 

from scipy.linalg import orth

import dynamics1D.potential as potential
from dynamics1D.quantum.continuous import *
from scriptsOlympe.inputparams import *
from mpl_toolkits.mplot3d import Axes3D

# ~ self.Vnth=np.zeros(self.Vn.size,dtype=complex)

			
		# ~ data=np.load(spectrumfile)
		# ~ self.quasienergies=data['quasienergies'][:,0]
		# ~ self.beta=data['beta'][:,0]*2*np.pi
		# ~ data.close()
		# ~ self.Vn=np.fft.rfft(self.quasienergies)/self.N
		# ~ self.computeVnth()
				
				
	# ~ def computeVnth(self):
	
		# ~ diff=self.quasienergies-np.roll(self.quasienergies,-1)
		# ~ dbeta=0.5*(self.beta[1]-self.beta[0])

		# ~ ind= ((np.diff(np.sign(np.diff(np.abs(diff)))) < 0).nonzero()[0]+1)
		# ~ beta0=self.beta[ind]
		# ~ W=diff[ind]
		# ~ ind=beta0>0
		# ~ beta0=beta0[ind]+dbeta
		# ~ W=W[ind]
		
		# ~ n=np.arange(self.Vn.size)
		# ~ n[0]=n[1]
		
		# ~ def Vnthfun(n,W,beta0):
			# ~ return -W/(np.pi*n)*np.sin(beta0*n)
		
		# ~ for i in range(0,W.size):
			# ~ self.Vnth+=Vnthfun(n,W[i],beta0[i])
		
		# ~ self.Vnth[0]=self.Vn[0]	

# ~ wdir="/tmpdir/p0110mm/new/longrange/"
wdir="../../data/new/longrange/"
# ~ spectrum="spectrum-beta-g0d20-e0d00-hm0d45-338962"
spectrum="spectrum-beta-g0d20-e0d15-hm0d45-338961"
spectrum="spectrum-beta-g0d20-e0d15-hm0d45-339193"
spectrum="spectrum-beta-1resonance-339894"

params=readInput(wdir+spectrum+"/params.txt")
h=float(params['h'])
e=float(params['epsilon'])
gamma=float(params['gamma'])
Npcell=int(params['Npcell'])

data=np.load(wdir+spectrum+"/data.npz")
beta=data['beta'][:,0]
quasienergy=data['quasienergies'][:,0]
data.close()

print(Npcell,gamma,e,h)


Ncell=5
N=Npcell*Ncell

print(Ncell)

grid=Grid(N,h,xmax=Ncell*2*np.pi)
pot=potential.ModulatedPendulum(e,gamma)


floquet=FloquetPropagator(grid,pot,T0=4*np.pi,idtmax=1000,beta=0.001)

wf=WaveFunction(grid)
wf.setState("coherent",2.5)


# ~ floquet.diagonalize()
# ~ floquet.orderEigenstatesWithOverlapOn(wf)

beta0=np.zeros(Ncell)


cmap= plt.cm.get_cmap('nipy_spectral')


# ~ ax=plt.gca()

# ~ for i in range(Ncell):
	# ~ I=np.sum(np.conjugate(floquet.eigenvec[i].x)*np.roll(floquet.eigenvec[i].x,Npcell))*grid.dx
	# ~ beta0[i]=np.angle(I)
	# ~ ax.plot(grid.x,np.real(floquet.eigenvec[i].x),c=c)
	
# ~ plt.show()

Vn=np.fft.rfft(quasienergy)/quasienergy.size
Vn=np.delete(Vn,0)
print(Vn.size)

ax=plt.subplot(1,2,1)
# ~ ax.set_ylim(-0.1080,-0.1045)
# ~ ax.set_ylim(-0.1067,-0.1057)
ax.set_xlim(-np.pi,np.pi)
ax.set_xticks([-np.pi,0,np.pi])
ax.set_yticks([])
ax.set_xlabel(r"Quasi-momentum $\beta$")
ax.set_ylabel("Energy")
ax.set_xticklabels([r"-$\pi/\lambda$",r"0",r"$\pi/\lambda$"])
# ~ ax.scatter(beta0,floquet.quasienergy[0:Ncell],c="red",zorder=1)
ax.scatter(beta*2*np.pi,quasienergy,c="blue",zorder=0,s=0.7)

ax=plt.subplot(1,2,2)
# ~ ax.plot(np.arange(Vn.size)+1,np.abs(Vn),c="blue")
ax.plot(np.arange(Vn.size)+1,np.abs(Vn),c="red")
ax.set_yscale('log')
ax.set_xlim(0,300)
ax.set_ylim(10**(-8),10**(-3))
ax.set_xlabel(r"Distance between sites $n$")
ax.set_ylabel(r"Coupling $V_n$")

plt.show()


bt=(beta0+np.pi)/(2*np.pi)
# ~ print(bt)
# ~ ax=plt.gca()
# ~ ax.set_xlabel(r"$k/(2\pi/\lambda)$")
# ~ ax.set_ylabel(r"$\langle k | \beta \rangle$")
# ~ ax.set_xlim(-5,5)
# ~ for i in range(Ncell):
	# ~ ax.plot(np.fft.ifftshift(grid.p)/grid.h,np.fft.ifftshift(np.abs(floquet.eigenvec[i].p)**2),c=cmap(bt[i]))
# ~ plt.show()

# ~ fig=plt.figure()


# ~ ax = plt.subplot(111,projection='3d')
# ~ ax.get_proj = lambda: np.dot(Axes3D.get_proj(ax), np.diag([2.5, 1, 1, 2]))
#ax.set_xlabel(r"$x/\lambda$")
# ~ ax.set_ylabel(r"$Im(\langle x|\beta \rangle)$")
# ~ ax.set_zlabel(r"$Re(\langle x|\beta \rangle)$")

# ~ ax.set_yticks([])
# ~ ax.set_zticks([])
#ax.set_xticks(np.arange(Ncell+1)-int(0.5*(Ncell-1))-0.5)
# ~ ax.set_xticks([])
# ~ ax.set_xlim(-0.5*(Ncell),0.5*(Ncell))

# ~ for i in range(Ncell):
	# ~ ax.plot(grid.x/(2*np.pi),np.imag(floquet.eigenvec[i].x),np.real(floquet.eigenvec[i].x),c=cmap(bt[i]),zorder=int(Ncell-i))



plt.show()
	

