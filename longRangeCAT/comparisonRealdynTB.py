import sys
sys.path.insert(0, '..') 
import numpy as np

from dynamics1D.quantum import *
import dynamics1D.potential as potential

from scriptsOlympe.inputparams import *

import matplotlib.pyplot as plt
import matplotlib.patches as patches

# ~ wdir="/tmpdir/p0110mm/new/longrange/"
wdir="../../data/new/longrange/"


spectrumregular="spectrum-beta-g0d20-e0d00-hm0d45-338962"
spectrumchaotic="spectrum-beta-g0d20-e0d15-hm0d45-338961"

# ~ spectrumchaotic="spectrum-beta-g0d20-e0d15-hm0d45-339193"

# ~ spectrumregular="spectrum-beta-g0d15-e0d00-h0d4-338807"
# ~ spectrumchaotic="spectrum-beta-g0d15-e0d50-h0d4-338802"

mode="computetb"
mode="computereal"
mode="plot"

dfile="tb-harmconf"

if mode=="computetb":

	params=readInput(wdir+spectrumchaotic+"/params.txt")
	h=float(params['h'])
	N=int(params['nruns'])


	lattice=Lattice(N,h)

	tb=TightBindingHamiltonian(lattice,wdir+spectrumchaotic+"/data.npz")
	wf=QuantumState(lattice)
	wf.setState("dirac",0)
	
	
	mu=0.000001
	for i in range(N):
		tb.M[i,i]=tb.M[i,i]+mu*(i-lattice.ncenter)**2
		# ~ print(tb.M[i,i])
	
	# ~ for i in range(N):
		# ~ tb.M[i,i]=tb.M[i,i]

	tp=DiscreteTimePropagator(lattice,4*np.pi,tb)

	tmax=5000
	prob=np.zeros((tmax,N))

	for it in range(tmax):
		# ~ print(it)
		prob[it]=np.abs(wf.n)**2
		prob[it]/=np.max(prob[it])
		tp.propagate(wf)
		
	np.savez("tempdata/"+dfile,prob=prob,time=np.arange(tmax))
	
if mode=="computereal":
	params=readInput(wdir+spectrumregular+"/params.txt")
	h=float(params['h'])
	Npcell=int(params['Npcell'])
	Ncell=int(params['nruns'])
	
	params=readInput(wdir+spectrumchaotic+"/params.txt")
	e=float(params['epsilon'])
	gamma=float(params['gamma'])
	
	
	print(e,h,gamma,Npcell)
	
	data=np.load(wdir+spectrumregular+"/data.npz")
	wannierx=data['wannierx']
	data.close()
	
	N=Npcell*Ncell	
	
	grid=Grid(N,h,Ncell*2*np.pi)
	wf=WaveFunction(grid)
	wf.x=wannierx
	wf.normalize("x")

	pot=potential.ModulatedPendulum(e,gamma)
	pot=potential.ModulatedPendulumHarmonicConfinment(e,gamma,0.000001)
	floquet=FloquetPropagator(grid,pot,T0=4*np.pi,idtmax=1000)
	
	tmax=5000
	
	wf=WaveFunction(grid)
	wf.x=np.abs(wannierx)
	wf.normalize("x")
	
	
	ax=plt.gca()
	wfcell=[]
	for i in range(Ncell):
		wfcell.insert(i,WaveFunction(grid))
		wfcell[i].x=np.roll(wf.x,(i-int(0.5*(Ncell-1)))*Npcell)
		ax.plot(grid.x/(2*np.pi),np.abs(wfcell[i].x)**2)
	plt.show()
	

	
	
	prob=np.zeros((tmax,Ncell))
	
	ax=plt.gca()
	for it in range(0,tmax):
		print(it)
		prob[it]=np.array([wf//wfi for wfi in wfcell])
		prob[it]/=np.max(prob[it])
		floquet.propagate(wf)
		
		
	np.savez("tempdata/real",prob=prob,time=np.arange(tmax))
	

if mode=="plot":
	ax=plt.subplot(2,1,1)
	
	data=np.load("tempdata/"+dfile+".npz")
	time=data['time']
	probTB=data['prob']	
	data.close()
	Ncell=probTB[0].size

	ax.set_ylim(-30,30)
	
	times,n=np.meshgrid(time,np.arange(Ncell)-int(0.5*(Ncell-1)))
	probTB=np.swapaxes(probTB,0,1)
	levels = np.linspace(0,1,10,endpoint=True)
	im=ax.contourf(times,n,probTB, levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax.set_ylabel(r"$\langle n | \psi(t) \rangle$")
	
	ax=plt.subplot(2,1,2)
	
	data=np.load("tempdata/real.npz")
	time=data['time']
	probReal=data['prob']	
	data.close()
	Ncell=probReal[0].size

	ax.set_ylim(-30,30)
	
	times,n=np.meshgrid(time,np.arange(Ncell)-int(0.5*(Ncell-1)))
	probReal=np.swapaxes(probReal,0,1)
	levels = np.linspace(0,1,10,endpoint=True)
	im=ax.contourf(times,n,probReal, levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	ax.set_xlabel("Time (in periods)")
	ax.set_ylabel(r"$\langle n | \psi(t) \rangle$")
	
	# ~ ax=plt.subplot(2,2,3)
	
	# ~ levels = np.linspace(0,1,10,endpoint=True)
	# ~ im=ax.contourf(times,n,np.abs(probReal-probTB)/np.abs(probReal), levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	# ~ ax=plt.subplot(2,2,4)
	
	# ~ levels = np.linspace(0,1,10,endpoint=True)
	
	# ~ levels = np.linspace(0,np.max(np.abs(probReal-probTB)),10,endpoint=True)
	# ~ im=ax.contourf(times,n,np.abs(probReal-probTB), levels=levels, cmap='gnuplot2',extent=(0,0,np.max(time),Ncell))
	
	
	plt.show()
