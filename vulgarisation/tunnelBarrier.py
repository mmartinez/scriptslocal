import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

# ~ from inputparams import *
from dynamics1D import potential
from dynamics1D.quantum import *


h=0.1
gamma=0.07/(2*np.pi)
x0=0.1

N=64*8*2
grid=Grid(N,h,xmax=120)

pot=potential.Square(-gamma,x0)
tp=TimePropagator(grid,pot,T0=1,idtmax=1000)

wf=WaveFunction(grid)
wf.setState("coherent",20)
wf.shift("x",-40)
wf.shift("p",10)


for i in range(120):
	print(i)
	tp.propagate(wf)


	if i%2==0:
		ax=plt.gca()
		
		ax.set_xlim(-60,60)
		ax.set_xticks([])
		ax.set_yticks([])
		ax.plot(grid.x,pot.Vx(grid.x),c="black")
		ax=ax.twinx()
		ax.set_xlim(-60,60)
		ax.set_xticks([])
		ax.set_yticks([])
		ax.plot(grid.x,np.abs(wf.x)**2,c="red")
		
		fig=plt.gcf()
		fig.subplots_adjust(left=0.05, bottom=0.05, right=0.95, top=0.95)
		
		plt.savefig("temp/{:05d}.png".format(int(i/2)))
		plt.clf()

os.system("convert -delay 10 temp/*.png tunnelBarrier.gif")	
os.system("rm -r temp/*")




