import sys
import os
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

# ~ from inputparams import *
from dynamics1D.potential import *
from dynamics1D.quantum import *


h=0.2
gamma=0.25
e=1.0

N=64*2
grid=Grid(N,h)

pot=DoubleWell(e,gamma)

ham=Hamiltonian(grid,pot)
ham.diagonalize()

print("Expected frequency =",2*np.pi*h/(np.abs(ham.eigenval[0]-ham.eigenval[1])))

wf=(ham.eigenvec[0]+ham.eigenvec[1])/np.sqrt(2)
tp=TimePropagator(grid,pot,T0=1,idtmax=1000)

# ~ print(np.max(np.abs(wf.x)**2))
# ~ print(np.sum(np.abs(wf.x)**2))

# ~ wf=WaveFunction(grid)
# ~ wf.setState("coherent",1)
# ~ wf.shift("x",-1.33)
# ~ wf.shift("p",10)

ax=plt.gca()
ax.set_xlim(-np.pi,np.pi)
ax.set_xticks([])
ax.set_yticks([])
ax.plot(grid.x,pot.Vx(grid.x),c="black")
ax=ax.twinx()
ax.set_xlim(-np.pi,np.pi)
ax.set_ylim(-0.005,0.06)
ax.set_xticks([])
ax.set_yticks([])
ax.plot(grid.x,np.abs(wf.x)**2,c="red")
plt.show()

for i in range(925):
	print(i)
	tp.propagate(wf)


	if i%25==0:
		ax=plt.gca()
		
		ax.set_xlim(-np.pi,np.pi)
		ax.set_xticks([])
		ax.set_yticks([])
		ax.plot(grid.x,pot.Vx(grid.x),c="black")
		ax=ax.twinx()
		ax.set_xlim(-np.pi,np.pi)
		ax.set_ylim(-0.005,0.06)
		ax.set_xticks([])
		ax.set_yticks([])
		ax.plot(grid.x,np.abs(wf.x)**2/3.5,c="red")
		
		fig=plt.gcf()
		fig.subplots_adjust(left=0.05, bottom=0.05, right=0.95, top=0.95)
		
		plt.savefig("temp/{:05d}.png".format(int(i/2)))
		plt.clf()

os.system("convert -delay 10 temp/*.png tunnelDoubleWell.gif")	
os.system("rm -r temp/*")







